import React from "react";
import "./App.css";

//Include Sweetalert
import Swal from 'sweetalert2'

//axios for api request
import axios from 'axios';

  class App extends React.Component {
    constructor(props)
      {
        super(props);
        this.addFormData = this.addFormData.bind(this);
      }
    //Form Submission
    addFormData(evt)
      {
        evt.preventDefault();
        const fd = new FormData();
        fd.append('fullName', this.refs.fullName.value);
        fd.append('latestDegree', this.refs.latestDegree.value);
        fd.append('stdCGPA', this.refs.stdCGPA.value);
        fd.append('userCV', this.refs.userCV.value);
        fd.append('feild', this.refs.feild.value);
        fd.append('skills', this.refs.skills.value);
        
        axios.post('http://localhost/reactassesment/insert.php', fd
        ).then(res=>
        {
         
           //Success alert
         Swal.fire({
          title: 'Form Submitted',
          text: res.data.data,
          icon: 'success',
          
        });
      this.myFormRef.reset();
      
      }
      );
      }
   
    render() {
     
      return (
        <div className="App">   
          
  
        <form class="box" ref={(el) => this.myFormRef = el}>
          <input type="text" ref="fullName" placeholder="Name" required/>
          <input type="text" ref="latestDegree" placeholder="last/Latest Degree" required/>
          <input type="number" ref="stdCGPA" min="2.50" max="4.0" step="0.01" placeholder="CGPA" required/>
          <label htmlFor=""><p>CV file upload</p>
          <input type="file" ref="userCV"  placeholder="" required/>
          </label>
    
          {<label  for="post" placeholder=""><p>Select post you applied for.</p>
          <select ref="feild" required >
          <option value="Front-End">Front-End</option>
          <option value="Back-End">Back-End</option>
          <option value="Full Stack Developer">Full Stack Developer</option>
          <option value="Maketing">Maketing</option>
          </select>
          </label>}
         
          {<label className="design" > <p>Select more than two skills</p> 
          <input type="checkbox" ref="skills"  value="html"/> <label htmlFor="">HTML</label> <br />
          <input type="checkbox" ref="skills"  value="css"/> <label htmlFor="">CSS</label> <br />
          <input type="checkbox" ref="skills"  value="javascript"/> <label htmlFor="">JAVASCRIPT</label> <br />
          <input type="checkbox" ref="skills"  value="bootstrap"/> <label htmlFor="">BOOTSTRAP</label> <br />
          <input type="checkbox" ref="skills"  value="github"/> <label htmlFor="">GITHUB</label> <br />
          </label>}
    
          <button type="submit"  onClick={this.addFormData}>Submit</button>

          
        </form>
    
        </div>
   )
  };
  }
  
  export default App;