
import React from "react";

// const app=require('express')(); app.use(cors())
var url = "http://localhost/reactassesment/fetchresults.php";
class Display extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = { results: [] };
      }
    
  componentDidMount() {
    fetch( url, 
        {
            mode: "no-cors",
            method: 'POST',
            header:{
                // 'Access-Control-Allow-Origin': '*',
                'Content-Type':'application/json',
                'Accept':'application/json'
            },
        }
        )
      .then(response => response.json() )
      .then(results => console.log(results) )
      .catch(err => console.log(err) );
  }

  render() {
    return (
      <div>
        <ul>
          {this.state.results.map(result => (
            <p>
              <li>Full Name: {result.Fullname}</li>
              <li>Latest Degree: {result.LatestDegree}</li>
              <li>CGPA: {result.CGPA}</li>
              <li>UserCV: {result.UserCV}</li>
              <li>Feild: {result.Feild}</li>
              <li>Skills: {result.Skills}</li>
            </p>
          ))
          }
        </ul>
      </div>
    );
  }
}

export default Display;