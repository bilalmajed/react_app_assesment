import {Tabs, Tab, AppBar} from "@material-ui/core"
import {BrowserRouter,Route,Switch,Link} from "react-router-dom"
import UserView from "./userview"
import AdminView from "./adminview"
import "./bar.css"

export default function App(){
  const routes = ["/userview","/adminview"];

  return (
    <div className="App">
      <BrowserRouter>
    <Route path="/" render={(history)=>(
          <AppBar> 
          <Tabs value={history.location.pathname !=="/"
            ? history.location.pathname
            : false
          }>
            <Tab label="User View" value={routes[0]} component={Link} to={routes[0]} />
            <Tab label="Admin View" value={routes[1]} component={Link} to={routes[1]} />
          </Tabs>
        </AppBar>
    )}

    />

      <Switch>
        <Route path="/userview" component={UserView}/>
        <Route path="/adminview" component={AdminView}/>

      </Switch>


  </BrowserRouter>
    </div>
  );

}
